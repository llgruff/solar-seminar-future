Курс программирования на языке Scala, 2022

Семинар 2022.04.13  
Тема — Практика по использованию Future/Promise.

Посмотрите описание в [документации](https://docs.scala-lang.org/overviews/core/futures.html):
- scala.concurrent.{ExecutionContext, Future, Promise, Await, blocking}
- java.util.concurrent.{Executor, Executors}

Расписание всего курса + даты выхода лекций + ссылки на все видео и слайды можно найти [тут](https://maxcom.github.io/scala-course-2022/).
