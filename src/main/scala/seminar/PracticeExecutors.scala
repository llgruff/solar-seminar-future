package seminar

import java.time.LocalTime
import java.time.temporal.ChronoUnit
import java.util.concurrent.{Executor, Executors, ForkJoinPool, ScheduledExecutorService, TimeUnit}
import java.lang.Runnable

import scala.concurrent.{ExecutionContext, Future, Promise, Await, blocking}
import scala.concurrent.duration._

/*
Примеры:
PracticeExecutorsCommon
- смотрим общие функции
PracticeExecutorsOverflow
- запустили 16 blockingOperation
PracticeExecutorsBlocking
- запустили 16 blockingOperation
- запустили 260 blockingOperation
- System.setProperty("scala.concurrent.context.maxExtraThreads", "300")
- blocking {} исп. не во всех реализациях EC
PracticeExecutorsCustomEC
- запустили 1000 blockingOperation
- запустили 100_000 blockingOperation
PracticeExecutorsAsync
- 1_000_000
ExecutionContext.parasitic
Play Framework (Akka HTTP)
  https://doc.akka.io/docs/akka/current/dispatchers.html
*/

trait PracticeExecutorsCommon {

  private def time = LocalTime.now().truncatedTo(ChronoUnit.SECONDS)

  def printlnAsleep(id: Int, d: Duration, thread: Long): Unit =
    println(f"$time [thread:$thread%6d] [operation:$id%6d] Going asleep for ${d.toSeconds}s")

  def printlnWokeUp(id: Int, thread: Long): Unit =
    println(f"$time [thread:$thread%6d] [operation:$id%6d] Woke up")

  def runBlocking(id: Int, d: Duration): Int = {
    val thread: Long = Thread.currentThread().getId
    printlnAsleep(id, d, thread)
    Thread.sleep(d.toMillis)
    printlnWokeUp(id, thread)
    id
  }

  def somethingUseful(implicit ec: ExecutionContext): Future[Int] =
    Future {
      val thread: Long = Thread.currentThread().getId
      Console.err.println(f"$time [thread:$thread%6d] Doing something useful")
      2 + 2
    }

}


object PracticeExecutorsOverflow extends App with PracticeExecutorsCommon {

  def blockingOperation(id: Int, d: Duration)(implicit ec: ExecutionContext) =
    Future {
      runBlocking(id, d)
    }

  def blockingOps(implicit ec: ExecutionContext) =
    Future.traverse(1.to(16).toVector) {
      blockingOperation(_, 10.seconds)
    }

  import scala.concurrent.ExecutionContext.Implicits.global

  val bo = blockingOps
  val su = somethingUseful

  Await.result(bo, Duration.Inf)
  Await.result(su, Duration.Inf)

}


object PracticeExecutorsBlocking extends App with PracticeExecutorsCommon {

//  System.setProperty("scala.concurrent.context.maxExtraThreads", "300")

  def blockingOperation(id: Int, d: Duration)(implicit ec: ExecutionContext) =
    Future {
      blocking {
        runBlocking(id, d)
      }
    }

  def blockingOps(implicit ec: ExecutionContext) =
    Future.traverse(1.to(16).toVector) {
      blockingOperation(_, 10.seconds)
    }

  import scala.concurrent.ExecutionContext.Implicits.global

  val bo = blockingOps
  val su = somethingUseful

  Await.result(bo, Duration.Inf)
  Await.result(su, Duration.Inf)

}


object PracticeExecutorsCustomEC extends App with PracticeExecutorsCommon {

  def blockingOperation(id: Int, d: Duration)(implicit ec: ExecutionContext) =
    Future {
      blocking {
        runBlocking(id, d)
      }
    }

  def blockingOps(implicit ec: ExecutionContext) =
    Future.traverse(1.to(1000).toVector) {
      blockingOperation(_, 30.seconds)
    }

  val unboundedExecutor = Executors.newCachedThreadPool()
  val fixedExecutor     = Executors.newWorkStealingPool()

  val bo = blockingOps(ExecutionContext.fromExecutor(unboundedExecutor))
  val su = somethingUseful(ExecutionContext.fromExecutor(fixedExecutor))

  Await.result(bo, Duration.Inf)
  Await.result(su, Duration.Inf)

  unboundedExecutor.shutdown()
  fixedExecutor.shutdown()

}


object PracticeExecutorsAsync extends App with PracticeExecutorsCommon {

  class ScheduledEC(executor: ScheduledExecutorService) extends ExecutionContext {

    private val wrapped = ExecutionContext.fromExecutor(executor)

    def reportFailure(cause: Throwable): Unit = wrapped.reportFailure(cause)
    def execute(runnable: Runnable): Unit     = wrapped.execute(runnable)

    def delay[A](action: => A, d: Duration): Future[A] = {
      val p = Promise[A]()
      executor.schedule(
        () => p.success(action),
        d.toMillis,
        TimeUnit.MILLISECONDS
      )

      p.future
    }

  }

  def asyncOperation(id: Int, d: Duration)(implicit ec: ScheduledEC) =
    Future {
      val thread = Thread.currentThread().getId
      printlnAsleep(id, d, thread)
    }.flatMap { _ =>
      val thread = Thread.currentThread().getId
      ec.delay(printlnWokeUp(id, thread), d)
    }.map(_ => id)

  // 20 сек чтобы увидеть глазами паузу и сообщение о полезной работе не потерялось
  def asyncOps(implicit ec: ScheduledEC) =
    Future.traverse(1.to(1_000_000).toVector) {
      asyncOperation(_, 20.seconds)
    }

  val executor                 = Executors.newSingleThreadScheduledExecutor()
  implicit val ec: ScheduledEC = new ScheduledEC(executor)

  val ao = asyncOps
  val su = somethingUseful

  Await.result(ao, Duration.Inf)
  Await.result(su, Duration.Inf)

  executor.shutdown()

}

